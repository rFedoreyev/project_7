function getPrices() {
    return {
        type: [500, 1000, 1250],
        prodOptions: {
            option1 : 0,
            option2: 350,
            option3: 250,
        },
        prodProperties: {
            prop1: 1500,
            prop2: 2500,
        },
    };
}

function checkInp(){
   let inp = document.getElementById("kol").value;
    if(inp.match(/^[0-9]+$/) == null){
        document.getElementById("price").innerHTML = "Неверные данные";
        return false;}
    else    
        return true;
}

function price(){
    let sel = document.getElementById("type");
    let kol = document.getElementById("kol").value;
    let radioDiv = document.getElementById("radios");
    let boxes = document.getElementById("checkboxes");
    let price = document.getElementById("price");
    price.innerHTML = "0";
    
    switch (sel.value) {
        case "1":
            radioDiv.style.display = "none";
            boxes.style.display = "none";

            if(checkInp()){
                let all = parseInt(kol) * getPrices().type[0];
                price.innerHTML = all + " Rub";
            }
            break;
        case "2":
            radioDiv.style.display = "block";
            boxes.style.display = "none";
            let a;
            let rad = document.getElementsByName("prodOptions");
            for (var i = 0; i < rad.length; i++) {
                if (rad[i].checked){
                    a = rad[i].value;
                }
            }
            let priOfOp = getPrices().prodOptions[a];
            if(checkInp()){
                let all = parseInt(kol)*(getPrices().type[1] + priOfOp);
                price.innerHTML = all + " Rub";
            }   
            break;
        case "3":
            radioDiv.style.display = "none";
            boxes.style.display = "block";
            let sum = 0;
            let p = document.getElementsByName("prop");
            for (let i = 0; i < p.length ; i++) {
                if(p[i].checked){
                    sum += getPrices().prodProperties[p[i].value];
                }
            }

            if(checkInp()){
                let all = parseInt(kol) * (getPrices().type[2] + sum);
                price.innerHTML = all + " Rub";
            }
            break;
    }
}

window.addEventListener("DOMContentLoaded", function (event) {
    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = "none";
    let boxes = document.getElementById("checkboxes");
    boxes.style.display = "none";

let kol = document.getElementById("kol");
    kol.addEventListener("change", function (event) {
        console.log("Kol was changed");
        price();
    });

let select = document.getElementById("type");
    select.addEventListener("change", function (event) {
        console.log("Edition was changed");
        price();
    });

let radios = document.getElementsByName("prodOptions");
    radios.forEach(function (radio) {
        radio.addEventListener("change", function (event) {
            console.log("Option was changed");
            price();
        });
    });
let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function (checkbox) {
        checkbox.addEventListener("change", function (event) {
            console.log("Option was changed");
            price();
        });
    });
    price();
});
